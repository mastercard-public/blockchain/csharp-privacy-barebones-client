using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Threading.Tasks;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.Builder;
using McMaster.Extensions.CommandLineUtils;
using MasterCard.Core;
using MasterCard.Core.Exceptions;
using MasterCard.Core.Model;
using MasterCard.Core.Security.OAuth;
using MasterCard.Api.Blockchain;
using Google.Protobuf;
using P001;

[assembly: OwinStartup(typeof(csharp_blockchain_barebones_privacy_client.Startup))]
namespace csharp_blockchain_barebones_privacy_client
{
  class Program
  {
    public static String HELP_TEXT = "";
    static int Main(string[] args)
    {
      var app = new CommandLineApplication();
      app.HelpOption("-h|--help");
      var kpOption = app.Option("-kp|--keystorePath <keystorepath>", "the path to your keystore (mastercard developers)", CommandOptionType.SingleValue);
      var spOption = app.Option("-sp|--storePass <storepassword>", "keystore password (mastercard developers)", CommandOptionType.SingleValue);
      var kaOption = app.Option("-ka|--keyAlias <keyalias>", "key alias (mastercard developers)", CommandOptionType.SingleValue);
      var ckOption = app.Option("-ck|--consumerKey <consumerkey>", "consumer key (mastercard developers)", CommandOptionType.SingleValue);
      var pfOption = app.Option("-pf|--protoFile <protofile>", "the path to the protobuf file", CommandOptionType.SingleValue);
      var vOption = app.Option("-v|--verbosity", "log mastercard developers sdk to console", CommandOptionType.NoValue);
      app.OnExecute(() =>
      {
        var kp = kpOption.HasValue() ? kpOption.Value() : null;
        var sp = spOption.HasValue() ? spOption.Value() : "keystorepassword";
        var ka = kaOption.HasValue() ? kaOption.Value() : "keyalias";
        var ck = ckOption.HasValue() ? ckOption.Value() : null;
        var pf = pfOption.HasValue() ? pfOption.Value() : "transaction.proto";
        var v = vOption.HasValue();
        new BlockchainConsoleApp().start(kp, sp, ka, ck, pf, v);
      });
      HELP_TEXT = app.GetHelpText();
      return app.Execute(args);
    }
  }

  public delegate bool validate(String input);

  class BlockchainConsoleApp
  {
    public static readonly String APPID = "P001";
    public static readonly String ENCODING = "base64";

    String ks, kp, ka, ck, pf;
    bool v;
    void initAPI()
    {
      Console.WriteLine("Initializing");
      String ks = getFilePath("the path to your keystore (mastercard developers)", this.ks);
      String kp = getInput("keystore password (mastercard developers)", this.kp);
      String ka = getInput("key alias (mastercard developers)", this.ka);
      String ck = getInput("consumer key (mastercard developers)", this.ck);
      String pf = getFilePath("the path to the protobuf file", this.pf);
      ApiConfig.SetAuthentication(new OAuthAuthentication(ck, ks, ks, kp));
      ApiConfig.SetDebug(this.v);
      ApiConfig.SetSandbox(true);
      Console.WriteLine("Initialized");
    }

    String getInput(String msg, String defaultValue)
    {
      return getInput(msg, defaultValue, (input) =>
      {
        var exists = input != null && 0 < input.Length;
        if (!exists)
        {
          Console.WriteLine("Please enter a valid value");
        }
        return exists;
      });
    }
    int getNumberInput(String msg, String defaultValue)
    {
      return Int32.Parse(getInput(msg, defaultValue, (input) =>
      {
        var exists = false;
        try
        {
          exists = input != null && 0 < input.Length && 0 < Int32.Parse(input);
        }
        catch (System.Exception)
        {
          //NOOP
        }
        if (!exists)
        {
          Console.WriteLine("Please enter a valid value");
        }
        return exists;
      }));
    }
    String getInput(String msg, String defaultValue, validate callback)
    {
      String ret = null;
      String prompt = null == defaultValue ? $"{msg}:" : $"{msg} (:{defaultValue}):";
      do
      {
        Console.Write(prompt);
        ret = Console.ReadLine();
        if (0 == ret.Length && null != defaultValue)
        {
          ret = defaultValue;
        }
      } while (!callback(ret));
      return ret;
    }

    public void waitForEnter(String title = "Press ENTER to continue")
    {
      getInput(title, null, (input) => { return true; });
    }

    String getFilePath(String msg, String defaultValue)
    {
      return getInput(msg, defaultValue, (input) =>
      {
        var exists = File.Exists(input);
        if (!exists)
        {
          Console.WriteLine("Please enter a valid file");
        }
        return exists;
      });
    }
    public byte[] hexStringToBytes(string str)
    {
      var bytes = new byte[str.Length / 2];
      for (int idx = 0; idx < bytes.Length; idx++)
      {
        string currentHex = str.Substring(idx * 2, 2);
        bytes[idx] = Convert.ToByte(currentHex, 16);
      }
      return bytes;
    }
    public void showMenu()
    {
      bool done = false;
      do
      {
        Console.WriteLine();
        Console.WriteLine(File.ReadAllText("menu.txt"));
        var option = getInput("Option", "0", (input) =>
        {
          return Int32.Parse(input) >= 0 && Int32.Parse(input) <= 6;
        });
        switch (option)
        {
          case "0":
            done = true;
            break;
          case "1":
            updateNode();
            break;
          case "2":
            startModeratorNode();
            break;
          case "3":
            startSenderNode();
            break;
          case "4":
            createTransactionEntity();
            break;
          case "5":
            retrieveTransactionEntity();
            break;
          case "6":
            Console.WriteLine(Program.HELP_TEXT);
            break;
          default:
            Console.WriteLine("Unknown option");
            break;
        }
        if (!done)
        {
          waitForEnter();
        }
      } while (!done);
      Console.WriteLine("Goodbye");
    }
    public void updateNode()
    {
      Console.WriteLine("updateNode");
      String pf = getFilePath("Protobuf File", this.pf);
      try
      {
        RequestMap map = new RequestMap();
        map.Set("id", APPID);
        map.Set("name", APPID);
        map.Set("description", "");
        map.Set("version", 0);
        map.Set("definition.format", "proto3");
        map.Set("definition.encoding", ENCODING);
        map.Set("definition.messages", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText(pf))));
        App app = new App(map).Update();
        Console.WriteLine("app--> {0}", app);
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void startModeratorNode()
    {
      Console.WriteLine("startModeratorNode");
      var port = getNumberInput("Listening port", "8080");
      try
      {
        using (IDisposable app = WebApp.Start($"http://localhost:{port}/"))
        {
          waitForEnter("(press return at any time to stop server)");
          Console.WriteLine("(stopped server)");
        }
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void startSenderNode()
    {
      Console.WriteLine("startSenderNode");
      var from = getInput("From", "iban:1234");
      var to = getInput("To", "iban:5678");
      var amt = getNumberInput("Amount", "1234");
      var port = getNumberInput("Listening port", "8080");
      try
      {
        HttpClient client = new HttpClient();

        var response = client.PostAsJsonAsync($"http://localhost:{port}/payment", new TransactionRequest { From = from, To = to, Amount = amt }).Result;
        var txnResp = response.Content.ReadAsAsync<TransactionResponse>().Result;
        if (txnResp.Valid)
        {
          createTransactionEntity(new Transaction { From = from, To = to, Amount = amt, ModeratorRef = txnResp.ModeratorRef });
        }
        else
        {
          Console.Error.WriteLine("TransactionRequest was invalid");
        }
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void createTransactionEntity(Transaction txn = null)
    {
      Console.WriteLine("createTransactionEntity");
      if (null == txn)
      {
        var from = getInput("From", "iban:1234");
        var to = getInput("To", "iban:5678");
        var amt = getNumberInput("Amount", "1234");
        var modRef = getInput("Moderator Reference", "abc123");
        txn = new Transaction
        {
          From = from,
          To = to,
          Amount = amt,
          ModeratorRef = modRef
        };
      }
      try
      {
        RequestMap map = new RequestMap();
        map.Set("app", APPID);
        map.Set("encoding", ENCODING);
        map.Set("value", txn.ToByteString().ToBase64());
        TransactionEntry response = TransactionEntry.Create(map);
        Console.WriteLine("hash--> {0}", response["hash"]);
        Console.WriteLine("slot--> {0}", response["slot"]);
        Console.WriteLine("status--> {0}", response["status"]);
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void retrieveTransactionEntity()
    {
      Console.WriteLine("retrieveEntity");
      var hash = getInput("hash", null);
      try
      {
        RequestMap map = new RequestMap();
        map.Set("hash", hash);
        TransactionEntry response = TransactionEntry.Read("", map);

        Console.WriteLine("hash--> {0}", response["hash"]);
        Console.WriteLine("slot--> {0}", response["slot"]);
        Console.WriteLine("status--> {0}", response["status"]);
        Console.WriteLine("value--> {0}", response["value"]);
        Console.WriteLine("Decoded--> {0}", Transaction.Parser.ParseFrom(hexStringToBytes(response["value"].ToString())).ToString());
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void start(String ks, String kp, String ka, String ck, String pf, bool v)
    {
      this.ks = ks; this.kp = kp; this.ka = ka; this.ck = ck; this.pf = pf; this.v = v;
      Console.WriteLine();
      Console.WriteLine(File.ReadAllText("help.txt"));
      initAPI();
      updateNode();
      showMenu();
    }

  }

  public class TransactionRequest
  {
    public string From { get; set; }
    public string To { get; set; }
    public int Amount { get; set; }
  }
  public class TransactionResponse
  {
    public string ModeratorRef { get; set; }
    public bool Valid { get; set; }
  }

  public class BlockchainPoller
  {
    public static readonly int SLEEPTIME = 1000;

    public async void pollBlockchain(string hashToFind, long timeout)
    {
      bool done = false;
      string lastPolledBlock = null;
      Console.WriteLine("Beginning polling of blockchain for prospective transaction with hash {0}", hashToFind);
      while (!done)
      {
        RequestMap map = new RequestMap();
        if (null != lastPolledBlock)
        {
          map.Set("from", lastPolledBlock);
        }
        List<Block> responseList = Block.List(map);
        if (0 < responseList.Count)
        {
          responseList.ForEach((block) =>
          {
            lastPolledBlock = block["slot"].ToString();
            if (block["partitions"] is List<Dictionary<String, Object>>)
            {
              List<Dictionary<String, Object>> partitions = (List<Dictionary<String, Object>>)block["partitions"];
              partitions.ForEach((partition) =>
              {
                if (((List<Object>)partition["entries"]).Contains(hashToFind))
                {
                  Console.WriteLine("Transaction found at slot {0}", lastPolledBlock);
                  done = true;
                }
              });
            }
          });
          if (timeout < DateTime.UtcNow.Ticks)
          {
            Console.WriteLine("Entry not found within time limit, exiting poll.");
            done = true;
          }
          else if (!done)
          {
            Console.WriteLine("Entry not found, retrying.");
          }
        }
        else
        {
          Console.WriteLine("Empty block list.");
          done = true;
        }
        if (!done)
        {
          await Task.Delay(SLEEPTIME);
        }
      }
    }
  }

  public class PaymentController : System.Web.Http.ApiController
  {
    public static readonly String MODREF = "abc123def";
    public static readonly long TIMEOUT = 30 * 1000 * 10000;
    public TransactionResponse Post([FromBody]TransactionRequest value)
    {
      Transaction txn = new Transaction
      {
        From = value.From,
        To = value.To,
        Amount = value.Amount,
        ModeratorRef = MODREF
      };
      string hash = toHex(SHA256Managed.Create().ComputeHash(SHA256Managed.Create().ComputeHash(txn.ToByteArray()))).ToLower();
      Task.Run(() => new BlockchainPoller().pollBlockchain(hash, DateTime.UtcNow.Ticks + TIMEOUT));
      return new TransactionResponse { ModeratorRef = MODREF, Valid = true };
    }

    string toHex(byte[] data)
    {
      StringBuilder sb = new StringBuilder();
      for (int idx = 0; idx < data.Length; idx++)
      {
        sb.Append(data[idx].ToString("X2"));
      }
      return sb.ToString();

    }
  }


  public class Startup
  {
    public void Configuration(IAppBuilder appBuilder)
    {
      HttpConfiguration config = new HttpConfiguration();
      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "{controller}"
      );

      appBuilder.UseWebApi(config);
    }
  }

}
