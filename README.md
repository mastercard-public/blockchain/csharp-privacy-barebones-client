# csharp blockchain barebones privacy client #

## Getting Started ##
This application demonstrates a sample use-case of a 'B2B Transaction' application for the Mastercard Blockchain API. It showcases a 'Sender' node notifying a 'Moderator' node of an agreed transaction between two parties, with the Moderator then verifying and subsequently retrieving the written transaction from the blockchain for settlement. To get started you should take the following steps 

### Prerequisites ###
[.NET Core (windows for now)](https://www.microsoft.com/net/core)

**NOTE:Although this project uses dotnet core SDK, due to package dependencies. the app requires dotnet standard >=4.6**

 * Install protoc on your workstation (Google's protocol buffer compiler)
 * Clone this repository
 * Edit the message.proto file and assign your APP_ID for value of package
 * Goto Mastercard Developers and create a Mastercard Blockchain project (note this is currently a private API and you may need to request access). You will be taken through the wizard to create a node. You must provide an APP_ID and a protocol buffer definition i.e. message.proto.
 * You will receive a p12 file and a consumer key from Mastercard Developers for your project.
 * Execute the following commands
```bash
dotnet restore
dotnet run
```
When started it gets you to confirm your parameters and then displays a simple menu. 

## Menu ##
```
============ MENU ============
1. Update node
2. Mimic Moderator Node
3. Mimic Send Node
4. Create transaction entry
5. Retrieve transaction entry
6. Print Command Line Options
0. Quit

Option (:0):
```

Input '2' to begin listening for a transaction HTTP request as a 'moderator' node (note that the `localhost:8080` socket must be free).

You will then need to start a second instance of the application using the instructions above again. It can use the same consumer key and P12 file, although it does not need to.

Once the second application has started, input '3' to initiate a transaction to the 'moderator' node as a 'sender' node. You will see console output from both nodes as the transaction goes through multiple stages:

* The Sender sends a HTTP request to the Moderator with a hypothetical transaction agreed between two parties.
* The Moderator verifies this transaction (always successful for this demo app), and then returns a reference field unique to this transaction to the Sender (hardcoded in this case).
* The Moderator then generates a hash from all the transaction information it has and the reference field it has given, and immediately begins polling the Blockchain looking for this hash in anticipation of the Sender node writing it there.
* The Sender node writes its transaction with the reference field from the Moderator's response to the Blockchain.
* The Moderator eventually finds the entry written by the Sender in the Blockchain, and then settles this transaction via a Mastercard API (stubbed out in this app)


## More Commandline Options ##
```
Usage:  [options]

Options:
  -h|--help                          Show help information
  -kp|--keystorePath <keystorepath>  the path to your keystore (mastercard developers)
  -sp|--storePass <storepassword>    keystore password (mastercard developers)
  -ka|--keyAlias <keyalias>          key alias (mastercard developers)
  -ck|--consumerKey <consumerkey>    consumer key (mastercard developers)
  -pf|--protoFile <protofile>        the path to the protobuf file
  -v|--verbosity                     log mastercard developers sdk to console

```

## Useful Info ##
This project makes use of the Mastercard Blockchain SDK available from nuget.

```bash
dotnet add package MasterCard-Blockchain
```

### if you need to regenerate proto buf mapping (eg. transaction.proto) ###
```bash
protoc --proto_path=. --csharp_out=. transaction.proto
```